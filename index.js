const express = require("express");
const app = express();
const port = 5555;
app.get("/", (req, res) => {
  res.send("Hello world!");
});

app.listen(port, () => {
  console.log(`server run port ${port}`);
});
